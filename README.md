# PCMT Core Bundle for Akeneo Pim

## Key features

Core bundle is a bundle for PCMT. 

## Development
### Running Test-Suits
The PcmtCoreBundle is covered with tests and every change and addition has also to be covered with unit tests. It uses PHPUnit.

To run the tests you have to change to this project's root directory and run the following commands in your console:

```
make unit
```

### Coding style
PcmtCoreBundle the coding style can be checked with Easy Coding Standard.

```
make ecs
```
