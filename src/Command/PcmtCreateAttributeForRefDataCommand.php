<?php
/**
 * Copyright (c) 2019, VillageReach
 * Licensed under the Non-Profit Open Software License version 3.0.
 * SPDX-License-Identifier: NPOSL-3.0
 */

declare(strict_types=1);

namespace PcmtCoreBundle\Command;

use Akeneo\Pim\Structure\Bundle\Doctrine\ORM\Saver\AttributeSaver;
use Akeneo\Pim\Structure\Component\Factory\AttributeFactory;
use PcmtCoreBundle\Updater\AttributeUpdater;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class PcmtCreateAttributeForRefDataCommand extends Command
{
    /**
     * run inside terminal in fpm docker: bin/console $defaultName
     */
    /**
     * @var string
     */
    protected static $defaultName = 'pcmt:generate-ref-data-attr';

    /**
     * @var AttributeFactory
     */
    private $attributeFactory;

    /**
     * @var AttributeUpdater
     */
    private $attributeUpdater;

    /**
     * @var AttributeSaver
     */
    private $attributeSaver;

    public function __construct(AttributeFactory $attributeFactory, AttributeUpdater $attributeUpdater, AttributeSaver $attributeSaver)
    {
        $this->attributeFactory = $attributeFactory;
        $this->attributeUpdater = $attributeUpdater;
        $this->attributeSaver = $attributeSaver;
        parent::__construct();
    }

    public function configure(): void
    {
        parent::configure();
        $this->addArgument('ref-data-name', InputArgument::REQUIRED, 'The name of the reference data.');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $output->writeln(['Reference Data Attribute Creator', '============']);
        // retrieve the argument value using getArgument()
        $refDataName = $input->getArgument('ref-data-name');
        $output->writeln('Reference Data: ' . $refDataName);
        // create as Attribute
        $this->createAttributeForReferenceData($refDataName);
        $output->writeln('done');
        return Command::SUCCESS;
    }

    private function createAttributeForReferenceData(string $refDataName): void
    {
        // create attribute
        $gs1Attribute = $this->attributeFactory->create();
        // set attribute's data
        $this->attributeUpdater->update($gs1Attribute, [
            'code' => mb_strtolower($refDataName),
            'group' => 'technical',
            'reference_data_name' => $refDataName,
            'type' => 'pim_reference_data_simpleselect',
            'required' => false,
        ]);
        // save attribute into database
        $this->attributeSaver->save($gs1Attribute);
    }
}
