<?php

/**
 * Copyright (c) 2020, VillageReach
 * Licensed under the Non-Profit Open Software License version 3.0.
 * SPDX-License-Identifier: NPOSL-3.0
 */

declare(strict_types=1);

namespace PcmtCoreBundle\Twig;

use Akeneo\Platform\VersionProviderInterface;
use Oro\Bundle\ConfigBundle\Twig\ConfigExtension;
use Twig\TwigFunction;

class PcmtVersionExtension extends ConfigExtension
{
    /**
     * @var VersionProviderInterface
     */
    private $versionProvider;

    public function __construct(VersionProviderInterface $versionProvider)
    {
        $this->versionProvider = $versionProvider;
    }

    public function getFunctions(): array
    {
        return [new TwigFunction('pcmt_version', [$this, 'version'])];
    }

    public function version()
    {
        return $this->versionProvider->getFullVersion();
    }
}
