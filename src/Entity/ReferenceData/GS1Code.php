<?php
/**
 * Copyright (c) 2019, VillageReach
 * Licensed under the Non-Profit Open Software License version 3.0.
 * SPDX-License-Identifier: NPOSL-3.0
 */

declare(strict_types=1);

namespace PcmtCoreBundle\Entity\ReferenceData;

abstract class GS1Code
{
    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $sortOrder;

    /**
     * @var string
     */
    protected $listName;

    /**
     * @var string
     */
    protected $code;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $definition;

    /**
     * @var int
     */
    protected $version;

    /**
     * @var \DateTime
     */
    protected $changeDate;

    /**
     * @var int
     */
    protected $status = 1;

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getListName(): string
    {
        return $this->listName;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $codeName): void
    {
        $this->name = $codeName;
    }

    public function getDefinition(): string
    {
        return $this->definition;
    }

    public function setDefinition(?string $definition): void
    {
        $this->definition = $definition;
    }

    public function setChangeDate(\DateTime $changeDate): void
    {
        $this->changeDate = $changeDate;
    }

    public function getChangeDate(): \DateTime
    {
        return $this->changeDate;
    }

    public function getVersion(): int
    {
        return $this->version;
    }

    public function setVersion(int $version): void
    {
        $this->version = $version;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): void
    {
        $this->status = $status;
    }

    protected function setListName(string $listName): void
    {
        $this->listName = $listName;
    }
}
