<?php
/**
 * Copyright (c) 2019, VillageReach
 * Licensed under the Non-Profit Open Software License version 3.0.
 * SPDX-License-Identifier: NPOSL-3.0
 */

declare(strict_types=1);

namespace PcmtCoreBundle\Connector\Job\Step;

use Akeneo\Pim\Enrichment\Component\Product\Model\ProductInterface;
use Akeneo\Tool\Component\Batch\Item\InvalidItemException;
use Akeneo\Tool\Component\Batch\Item\ItemReaderInterface;
use Akeneo\Tool\Component\Batch\Item\ItemWriterInterface;
use Akeneo\Tool\Component\Batch\Model\StepExecution;
use Akeneo\Tool\Component\Batch\Step\ItemStep;

class MstSupplierExportStep extends ItemStep
{
    public const FAMILY_TO_CROSS_READ = 'MD_SUPPLIER_MASTER';

    protected ?ItemWriterInterface $writer = null;

    protected ?ItemReaderInterface $reader = null;

    /**
     * @var ProductInterface[]
     */
    private $productsFromFamilyToCrossJoin = [];

    public function doExecute(StepExecution $stepExecution): void
    {
        $this->initializeStepElements($stepExecution);
        $this->productsFromFamilyToCrossJoin = $this->getCrossProducts();
        parent::doExecute($stepExecution);
    }

    protected function write($processedItems): void
    {
        try {
            $this->writer->writeCross($processedItems, $this->productsFromFamilyToCrossJoin);
        } catch (InvalidItemException $e) {
            $this->handleStepExecutionWarning($this->stepExecution, $this->writer, $e);
        }
    }

    /**
     * @return ProductInterface[]
     */
    private function getCrossProducts(): array
    {
        $this->reader->setFamilyToCrossRead(self::FAMILY_TO_CROSS_READ);
        $itemsToWrite = [];
        while (true) {
            try {
                $readItem = $this->reader->readCross();
                if ($readItem === null) {
                    break;
                }
            } catch (InvalidItemException $e) {
                $this->handleStepExecutionWarning($this->stepExecution, $this->reader, $e);
                continue;
            }
            $processedItem = $this->process($readItem);
            if ($processedItem !== null) {
                $itemsToWrite[] = $processedItem;
            }
        }

        return $itemsToWrite;
    }
}
